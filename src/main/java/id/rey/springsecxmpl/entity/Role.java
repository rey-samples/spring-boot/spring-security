package id.rey.springsecxmpl.entity;

import jakarta.persistence.*;
import lombok.Getter;

import java.io.Serializable;

@Getter
@Entity
public class Role implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    private String roleName;

    public Role() {
    }

    public Role(Long id, User user, String roleName) {
        this.id = id;
        this.user = user;
        this.roleName = roleName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
