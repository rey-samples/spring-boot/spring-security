package id.rey.springsecxmpl.logging;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Component
public class LoggingServiceImpl implements LoggingService {
    private static final Logger log = Logger.getLogger(LoggingServiceImpl.class.getSimpleName());
    private static final String[] restrictedHeader = {"authorization", "user_key"};

    @Override
    public void logRequest(HttpServletRequest httpServletRequest, Object body) {
        try {
            Map<String, Object> mapRequest = new HashMap<>();
            mapRequest.put("logType", "REQUEST");
            mapRequest.put("method", httpServletRequest.getMethod());
            mapRequest.put("path", httpServletRequest.getRequestURI());
            mapRequest.put("headers", buildHeadersMapReq(httpServletRequest));
            ThreadLocal<Map<String, String>> context = ThreadLocal.withInitial(HashMap::new);
            mapRequest.forEach((k, v) -> context.get().put(k, String.valueOf(v)));
            if (httpServletRequest.getParameterNames() != null && httpServletRequest.getParameterNames().hasMoreElements()) {
                mapRequest.put("parameters", buildParametersMap(httpServletRequest));
                log.info(
                        ">>REQUEST[ method= " + httpServletRequest.getMethod() +
                                ", path= " + httpServletRequest.getRequestURI() +
                                ", headers= " + mapRequest.get("headers") +
                                ", parameters= " + mapRequest.get("parameters")
                );
            }
            if (body != null) {
                mapRequest.put("body", new ObjectMapper().writeValueAsString(body));
                log.info(
                        ">>REQUEST[ method= " + httpServletRequest.getMethod() +
                                ", path= " + httpServletRequest.getRequestURI() +
                                ", headers= " + mapRequest.get("headers") +
                                ", body= " + mapRequest.get("body")
                );

            }
        } catch (Exception e) {
            log.warning(e.getLocalizedMessage());
        }
    }

    @Override
    public void logResponse(HttpServletRequest httpServletRequest,
                            HttpServletResponse httpServletResponse, Object body) {
        try {
            Map<String, Object> mapResponse = new HashMap<>();
            mapResponse.put("logType", "RESPONSE");
            mapResponse.put("method", httpServletRequest.getMethod());
            mapResponse.put("path", httpServletRequest.getRequestURI());
            mapResponse.put("responseHeaders", buildHeadersMapRes(httpServletResponse));
            if (body != null)
                mapResponse.put("responseBody", new ObjectMapper().writeValueAsString(body));
            ThreadLocal<Map<String, String>> context = ThreadLocal.withInitial(HashMap::new);
            mapResponse.forEach((k, v) -> context.get().put(k, String.valueOf(v)));
            log.info(
                    "<<RESPONSE] method= " + httpServletRequest.getMethod() +
                            ", path= " + httpServletRequest.getRequestURI() +
                            ", responseHeaders= " + mapResponse.get("responseHeaders") +
                            ", responseBody= " + mapResponse.get("responseBody") + " "
            );
        } catch (Exception e) {
            log.warning(e.getLocalizedMessage());
        }
    }

    private Map<String, String> buildParametersMap(HttpServletRequest request) {
        return Collections.list(request.getParameterNames())
                .stream()
                .collect(Collectors.toMap(
                        name -> name,
                        request::getParameter));
    }

    private Map<String, String> buildHeadersMapReq(HttpServletRequest request) {
        Map<String, String> headers = Collections.list(request.getHeaderNames())
                .stream().collect(Collectors.toMap(
                        key -> key,
                        request::getHeader));
        for (String s : restrictedHeader) {
            headers.computeIfPresent(s, (k, v) -> {
                int mid = (v.length() < 50 ? (v.length() / 4) : 12);
                return v.substring(0, mid) + "..." + v.substring(v.length() - mid);
            });
        }
        generateIdHeader(headers);
        return headers;
    }

    private Map<String, String> buildHeadersMapRes(HttpServletResponse response) {
        Map<String, String> map = new HashMap<>();
        response.getHeaderNames().forEach(header -> map.put(header, response.getHeader(header)));
        return map;
    }

    private void generateIdHeader(Map<String, String> headers) {
        ThreadLocal<Map<String, String>> context = ThreadLocal.withInitial(HashMap::new);
        context.get().clear();
        context.get().put("X-REQUEST-ID",
                headers.computeIfAbsent("X-REQUEST-ID",
                        k -> UUID.randomUUID().toString()));
        context.get().put("X-CORRELATION-ID",
                headers.computeIfAbsent("X-CORRELATION-ID",
                        k -> UUID.randomUUID().toString()));
    }
}