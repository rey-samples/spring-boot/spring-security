package id.rey.springsecxmpl.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/home")
@Slf4j
public class HomeController {

    @GetMapping(value = "/my")
    public String home() {
        log.info("This is your home");
        return "This is my Home";
    }
}
