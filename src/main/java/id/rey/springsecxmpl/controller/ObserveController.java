package id.rey.springsecxmpl.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ObserveController {

    @GetMapping("/healthcheck")
    public String healthCheck() {
        return "Ok. Healthy";
    }

    @GetMapping("/memory-status")
    public String memoryStatus() {
        return "";
    }
}
