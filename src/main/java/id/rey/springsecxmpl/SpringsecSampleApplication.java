package id.rey.springsecxmpl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringsecSampleApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringsecSampleApplication.class, args);
	}
}
