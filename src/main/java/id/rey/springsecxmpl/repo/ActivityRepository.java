package id.rey.springsecxmpl.repo;

import id.rey.springsecxmpl.entity.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long> {
    /**
     * the relation with User entity dont forget to
     * public void userAction(User user, String action) {
     *     Activity activity = new Activity();
     *     activity.setUser(user);
     *     activity.setAction(action);
     *     activity.setTimestamp(LocalDateTime.now());
     *     activityRepository.save(activity);
     * }
     *
     * user.getActivities().add(activity);
     *
     */
}
