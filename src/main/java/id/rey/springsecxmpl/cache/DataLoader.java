package id.rey.springsecxmpl.cache;

import id.rey.springsecxmpl.entity.User;
import id.rey.springsecxmpl.repo.UserRepository;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class DataLoader {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public DataLoader(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostConstruct
    public void addUserAsynchronously() {
        List.of("Alice", "Bob", "Charlie", "Dave", "Eve")
                .forEach(name -> {
                    User user = new User();
                    user.setUsername(name);
                    user.setPassword(passwordEncoder.encode("123"));
                    user.setEnabled(true);
                    userRepository.save(user);
                });
        User rey = new User();
        rey.setUsername("rey");
        rey.setPassword(passwordEncoder.encode("rey"));
        rey.setEnabled(true);
        userRepository.save(rey);
        log.info("=============Users [LOADED]");
    }
}
