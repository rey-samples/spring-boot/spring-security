package id.rey.springsecxmpl.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import javax.sql.DataSource;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
public class BasicSecurityConfig {
	@Value( "${spring.security.user.name}" )
	private String username;
	
	@Value( "${spring.security.user.password}" )
	private String password;
    private static final String[] WHITELIST = {
            "/healthcheck",
            "/images/**",
            "/js/**",
            "/login",
            "/memory-status",
            "/swagger-resources/**",
            "/swagger-ui/**",
            "/v3/api-docs",
            "/webjars/**",
            "/h2-**/**"
    };

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        return http
                .authorizeHttpRequests(authz -> authz
                        .requestMatchers(WHITELIST).permitAll()
                        .anyRequest().authenticated()
                )
                .httpBasic(withDefaults()).build();
    }


    /**
     * enable this only if you dont have custom Userdetailservice Implementation
     *     @Bean
     *     public DataSource dataSource() {
     *         return new EmbeddedDatabaseBuilder()
     *                 .setType(EmbeddedDatabaseType.H2)
     *                 .build();
     *     }
     *
     *     @Bean
     *     public UserDetailsManager userDetailsService(DataSource dataSource) {
     *         JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager(dataSource);
     *         UserDetails user = User.builder()
     *                 .username("rey")
     *                 .password(passwordEncoder().encode("rey"))
     *                 .roles("ADMIN")
     *                 .build();
     *         jdbcUserDetailsManager.createUser(user);
     *         return jdbcUserDetailsManager;
     *     }
     */


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(4);
    }

}